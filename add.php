<?php
    $title = "Add Movie";
    include "./includes/top.inc.php";
?>
<a href="./index.php">Back to movies</a>
    <br>
    <br>
<h1>Add A New Movie</h1>
<form action="./addDo.php" method="post">
    <p>
        <label for="movie_title">Movie Title:</label>
        <input type="text" id="movie_title" name="movie_title" value="" maxlength="80" size="40">
    </p>
    <p>
        <label for="synopsis">Synopsis:</label>
    </p>
    <textarea name="synopsis" id="synopsis" cols="40" rows="5"></textarea>
    <p>
        <label for="release_date">Release Date:</label>
        <input type="text" id="release_date" name="release_date" value="" maxlength="20" size="20">
    </p>
    <p>
        <label for="rating">Rating:</label>
        <select name="rating" id="rating">
            <option value="1">1 - Very Poor</option>
            <option value="2">2 - Poor</option>
            <option value="3">3 - Ok</option>
            <option value="4">4 - Good</option>
            <option value="5">5 - Great</option>
        </select>
    </p>
    <input type="submit" name="submit" value="Add Movie">
</form>

<?php
    include_once "./includes/bottom.inc.php";
?>
