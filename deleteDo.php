<?php
    require_once './includes/dbconnect.inc.php';
    require_once './includes/functions.inc.php';

    $title = "Edit Movie";
    include_once "./includes/top.inc.php";
    session_start();

    $id = htmlspecialchars($_POST['id']);

    if(! is_numeric($id) || $id < 1 ){
        $msg = "Invalid ID given";
        redirect($msg);
    }

    $idSql = $mysqli->real_escape_string($id);

    $msg = deleteMovie($mysqli,$idSql);

    redirect($msg);
