<?php
    require_once './includes/dbconnect.inc.php';
    $title = "Delete Movie";
    include_once "./includes/top.inc.php";
    session_start();

    $id = htmlspecialchars($_GET['id']);

    if(! is_numeric($id) || $id < 1 ){
        $msg = "Invalid ID given";
        redirect($msg);
    }

    $idSql = $mysqli->real_escape_string($id);

    $sql = "SELECT id, movie_title, synopsis, release_date, rating FROM movies WHERE id=$idSql";

    $result = $mysqli->query($sql);


    if(!isset($result)){
        $msg = "Error retrieving record MySQL Error: " . $mysqli->error;
        redirect($msg);
    }
    else if ($result->num_rows != 1) {
        $msg = "Could not find record $id.";
        redirect($msg);;
    }

    $movie = $result->fetch_assoc();
?>
<h3>Do you want to delete <?php echo $movie['movie_title']; ?> </h3>
<form action="./deleteDo.php" method="post">
    <input type="hidden" id="id" name="id" value="<?php echo htmlspecialchars($id) ?>"/>
    <input type="submit" name="submit" value="Yes, Delete">
</form>
<br>
<a href='./index.php'><button>No, Don't Delete</button></a>
<?php
    include_once "./includes/bottom.inc.php";
?>
