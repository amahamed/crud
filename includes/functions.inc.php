<?php

    function sqlEscape($mysqli,$data){
        $movie = array();
        $movie['id'] = $mysqli->real_escape_string($data['id']);
        $movie['movie_title'] = $mysqli->real_escape_string($data['movie_title']);
        $movie['synopsis'] = $mysqli->real_escape_string($data['synopsis']);
        $movie['release_date'] = $mysqli->real_escape_string($data['release_date']);
        $movie['rating'] = $mysqli->real_escape_string($data['rating']);

        return $movie;
    }

    function addMovie($mysqli,$movie_title,$synopsis,$release_date,$rating){
        $sql = "INSERT INTO `movies` (`id`, `movie_title`, `synopsis`, `release_date`, `rating`) VALUES (0,'$movie_title','$synopsis','$release_date','$rating');";

        $ok = $mysqli->query($sql);

        if($ok){
            $id = $mysqli->insert_id;
            $msg = "Record # $id added!";
        }
        else {
            $msg = "Error creating record. MySQL Error: " . $mysqli->error;
        }

        return $msg;
    }

    function updateMovie($mysqli,$movie_title,$synopsis,$release_date,$rating,$id){
        $sql = "UPDATE movies SET `movie_title` = '$movie_title',`synopsis` = '$synopsis', `release_date` = '$release_date', `rating` = '$rating' WHERE `id` = '$id'";

        $ok = $mysqli->query($sql);

        if($ok){
            $msg = "Record # $id updated!";
        }
        else {
            $msg = "Error saving record. MySQL Error: " . $mysqli->error();
        }

        return $msg;
    }

    function deleteMovie($mysqli,$idSql){
        $sql = "DELETE FROM `movies` WHERE `id`=$idSql";

        $ok = $mysqli->query($sql);

        if($ok){
            $msg = "Record # $idSql deleted!";
        }
        else {
            $msg = "Error deleting record MySQL Error: " . $mysqli->error;
        }

        return $msg;
    }

    function getMovie($mysqli,$idSql){
        $sql = "SELECT id, movie_title, synopsis, release_date, rating FROM movies WHERE id=$idSql";

        $result = $mysqli->query($sql);

        if(!isset($result)){
            $msg = "Error retrieving record MySQL Error: " . $mysqli->error;
            $_SESSION['msg'] = $msg;
            header("Location: index.php");
            exit();
        }
        else if ($result->num_rows != 1) {
            $msg = "Could not find record $idSql.";
            $_SESSION['msg'] = $msg;
            header("Location: index.php");
            exit();
        }

        return $result;
    }

    function redirect($msg='No message')
    {
      session_start();
      $_SESSION['msg'] = $msg;
      header('Location: index.php');
      exit();
    }
