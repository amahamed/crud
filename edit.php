<?php
    require_once './includes/dbconnect.inc.php';
    require_once  './includes/functions.inc.php';

    $title = "Edit Movie";
    include_once "./includes/top.inc.php";
    session_start();

    $id = htmlspecialchars($_GET['id']);

    if(! is_numeric($id) || $id < 1 ){
        $msg = "Invalid ID given";
        redirect($msg);
    }

    $idSql = $mysqli->real_escape_string($id);

    $result = getMovie($mysqli,$idSql);


    $movie = $result->fetch_assoc();

?>
    <a href="./index.php">Back to movies</a>
    <br>
    <br>
<form action="./editDo.php" method="post">
    <p>
        <label for="movie_title">Movie Title:</label>
        <input type="text" id="movie_title" name="movie_title" value="<?php echo htmlspecialchars($movie['movie_title']) ?>" maxlength="80" size="40">
        <input type="hidden" id="id" name="id" value="<?php echo htmlspecialchars($id) ?>"/>
    </p>
    <p>
        <label for="synopsis">Synopsis:</label>
    </p>
    <textarea name="synopsis" id="synopsis" cols="40" rows="5"><?php echo htmlspecialchars($movie['synopsis']) ?></textarea>
    <p>
        <label for="release_date">Release Date:</label>
        <input type="text" id="release_date" name="release_date" value="<?php echo htmlspecialchars($movie['release_date']) ?>" maxlength="20" size="20">
    </p>
    <p>
        <label for="rating">Rating:</label>
        <select name="rating" id="rating">
            <option value="1">1 - Very Poor</option>
            <option value="2">2 - Poor</option>
            <option value="3">3 - Ok</option>
            <option value="4">4 - Good</option>
            <option value="5">5 - Great</option>
        </select>
    </p>
    <input type="submit" name="submit" value="Update Movie">
</form>
<br/>
<?php
    echo "<a href=./delete.php?id=",htmlspecialchars($id),"><button>Delete Record</button></a>";
    include "./includes/bottom.inc.php";
?>
