<?php

    require_once './includes/dbconnect.inc.php';
    require_once './includes/functions.inc.php';

    session_start();

    $movie = sqlEscape($mysqli,$_POST);

    $msg = updateMovie($mysqli,$movie['movie_title'],$movie['synopsis'],$movie['release_date'],$movie['rating'],$movie['id']);

    redirect($msg);
