<?php
    require_once "includes/dbconnect.inc.php";
    $title = "Movies";
    session_start();
    include "./includes/top.inc.php";
    if(isset($_SESSION['msg'])) {
        $msg = $_SESSION['msg'];
    }
    else{
        $msg = 'No Message';
    }
    echo $msg;
    $_SESSION['msg'] = "";
?>
<table>
    <tr>
        <th>ID</th>
        <th>Movie Title</th>
        <th>Synopsis</th>
        <th>Release Date</th>
        <th>Rating</th>
    </tr>
        <?php

        while ($row = $result->fetch_assoc()){
            echo "<tr>";
            echo "<td>" , htmlspecialchars($row['id']) , "</td>";
            echo "<td>", htmlspecialchars($row['movie_title']), "</td>";
            echo "<td>", htmlspecialchars($row['synopsis']) ,"</td>";
            echo "<td>", htmlspecialchars($row['release_date']) ,"</td>";
            echo "<td>", htmlspecialchars($row['rating']) ,"</td>";
            echo "<td><a href='./edit.php?id=" ,$row['id'] ,"'>Edit</a></td>";
            echo "</tr>";
        }

    ?>
</table>
<br>
<br>
<a href="./add.php">
    <button>Add Movie</button>
</a>

<?php
    include "./includes/bottom.inc.php";
?>