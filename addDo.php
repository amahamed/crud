<?php
    require_once "./includes/dbconnect.inc.php";
    require_once "./includes/functions.inc.php";

    session_start();

    if(!isset($_POST['submit'])){
        $msg = "Error";
        redirect('Location: index.php',$msg);
    }

    $movie = sqlEscape($mysqli,$_POST);

    $msg = addMovie($mysqli,$movie['movie_title'],$movie['synopsis'],$movie['release_date'],$movie['rating']);


    redirect($msg);
